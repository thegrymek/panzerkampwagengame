package panzerkamp.object.terrain;

import java.awt.event.KeyEvent;
import panzerkamp.main.Constans;
import panzerkamp.object.EreignisObjekts;

import maybach.collision.TestCollision;
import maybach.object.Shape;
import maybach.object.Sprite;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.*;
import java.lang.Double;
import java.net.URL;



public class PlainSquare extends Sprite implements EreignisObjekts {

	private boolean isBlockAble = true;
	private boolean isDestroyAble = true;
	
	private int gesundheit = 500;
	
	public PlainSquare(int x, int y, URL fileName)
	{
		this(x,y,fileName, false, false, 0);
	}
	
	public PlainSquare(int x, int y, URL fileName, boolean istBlockierBar)
	{
		this(x, y, fileName, istBlockierBar, false, 0);
	}
	
	
	public PlainSquare(int x, int y, URL fileName, boolean istBlockierBar, boolean istZerstorBar, int gesundheit)
	{
		super(x,y, fileName);
		
		this.isBlockAble = istBlockierBar;
		this.isDestroyAble = istZerstorBar;
		this.gesundheit = gesundheit;
		
		this.setSide(3);
		this.setCollisionPoints(new ArrayList<Point>());
		this.addCollisionPoints(new Point(x,y));
		this.addCollisionPoints(new Point(x,y));
		this.addCollisionPoints(new Point(x,y));
		this.addCollisionPoints(new Point(x,y));
	}
	
	  
	@Override
	public Map<String,String> getMsg(String type, Shape s, Map<String,String> dict)
	{
		
		HashMap <String, String> res = new HashMap<String, String>();
		if ( type == Constans.MSG_HIT )
		{
			if ( this.istBlockierBar() || this.istZerstorBar() )
			{
				res.put(Constans.MSG_DESTROY, "this");
				
			}
			if ( this.istZerstorBar() )
			{
				if ( dict.containsKey("schaden"))
				{
					double dmg = java.lang.Double.parseDouble(dict.get("schaden"));
					
					this.gesundheit -= dmg;
					
					if ( this.gesundheit < 0 && this.istZerstorBar() )
					{
						this.destroy();
					}
				}
				
			}
			
			return res;
		}
		
		if ( type == Constans.MSG_ROTATE || type == Constans.MSG_MOVE )
		{
			if ( this.istBlockierBar() )
			{
				res.put(Constans.MSG_OBJECT_ISBLOCKABLE, "this");
			}
			return res;
		}
		
		return res;
	}
	
	
	@Override
	public Map<String, String> getInfo()
	{
		HashMap<String,String> info = new HashMap<String,String>();
		
		info.put("winkel", String.valueOf(this.angle));
		info.put("letzteX", String.valueOf(this.x));
		info.put("letzteY", String.valueOf(this.y));
		info.put("name", this.getName());
		info.put("seite", String.valueOf(this.getSide()));
		return info;
		
	}
	
	@Override
	public boolean istBlockierBar() {
		return this.isBlockAble;
	}


	@Override
	public boolean istZerstorBar() {
		return this.isDestroyAble;
	}

	
	
	public void paint(Graphics g)
	{
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
//		for ( int i = 0; i < 4; i++)
//		{
//			Point p = this.getCollisionPoints().get(i);
//			Point p1 = this.getCollisionPoints().get((i+1)%4);
//			g2d.drawLine(p.x, p.y, p1.x, p1.y);
//		}
	}
	public void update(long dt)
	{
		super.update(dt);
		if ( dt <= 0 ) return;
		TestCollision.updateSquareKollision(this.getCollisionPoints(), new Point(this.x, this.y), 0, this.getImage(), false);
	}


}
