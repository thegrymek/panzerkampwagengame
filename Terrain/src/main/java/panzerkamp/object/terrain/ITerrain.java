/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panzerkamp.object.terrain;

import java.awt.Image;
import java.util.Map;
import maybach.engine.Engine;

/**
 *
 * @author thegrymek
 */
public interface ITerrain {

    Map<String, String> getInfo();

    Image getLogo();

    /**
     * @return the name
     */
    String getName();

    int getSx();

    int getSy();

    void read(String filename, Engine engine);

    void setLogo(Image img);

    boolean setMap();

    void setSx(int sx);

    void setSy(int sy);
    
}
