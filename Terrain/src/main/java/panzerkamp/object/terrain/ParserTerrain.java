//package panzerkamp.object.terrain;
//
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.DocumentBuilder;
//
//import maybach.engine.Engine;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.NodeList;
//import org.w3c.dom.Node;
//import org.w3c.dom.Element;
//
//import panzerkamp.main.Constans;
//
//import java.io.File;
//import java.io.InputStream;
//
//public class ParserTerrain {
//
//    public static void read(String filename, Engine engine) {
//
//        try {
//
//            InputStream fXmlFile = Engine.class.getClass().getClassLoader().getResourceAsStream(filename);
////            File fXmlFile = new File()
////            this.getClass().getClassLoader().getResource(filename)
//            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//            Document doc = dBuilder.parse(fXmlFile);
//            doc.getDocumentElement().normalize();
//
//            NodeList nList = doc.getElementsByTagName("kobold");
//
//            for (int temp = 0; temp < nList.getLength(); temp++) {
//
//                Node nNode = nList.item(temp);
//                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//                    Element eElement = (Element) nNode;
//
//                    registerPlain(engine, eElement, Constans.LAYER_TERRAIN);
//                }
//            }
//
//            nList = doc.getElementsByTagName("himmel");
//
//            for (int temp = 0; temp < nList.getLength(); temp++) {
//
//                Node nNode = nList.item(temp);
//                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//                    Element eElement = (Element) nNode;
//
//                    registerPlain(engine, eElement, Constans.LAYER_SKY);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private static String getTagValue(String sTag, Element eElement) {
//        try {
//            NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
//
//            Node nValue = (Node) nlList.item(0);
//
//            return nValue.getNodeValue();
//        } catch (Exception e) {
//            return "null";
//        }
//    }
//
//    private static void registerPlain(Engine e, Element eElement, String layerName) {
//        String graphics_path = getTagValue("grafik", eElement);
//        int x = Integer.parseInt(getTagValue("posX", eElement));
//        int y = Integer.parseInt(getTagValue("posY", eElement));
//        boolean isBlockAble = false;
//        boolean isDestroyAble = false;
//        int gesundheit = 0;
//
//        if (getTagValue("beweglich", eElement).equalsIgnoreCase("falsch")) {
//            isBlockAble = true;
//        }
//
//        if (getTagValue("istzerstoren", eElement).equalsIgnoreCase("wahr")) {
//            isDestroyAble = true;
//        }
//
//        if (!getTagValue("lebenspunkte", eElement).equalsIgnoreCase("null")) {
//            gesundheit = Integer.parseInt(getTagValue("lebenspunkte", eElement));
//
//        }
//        e.RegisterShape(new PlainSquare(x, y, Constans.PATH_GRAPHICS_TERRAIN + graphics_path, isBlockAble, isDestroyAble, gesundheit), layerName);
//    }
//
//}
