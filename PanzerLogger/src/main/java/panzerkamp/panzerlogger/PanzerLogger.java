package panzerkamp.panzerlogger;

import java.util.ArrayList;
import java.util.List;
import panzerkamp.logger.LogEvent;
import panzerkamp.logger.LogListener;
import panzerkamp.logger.Logger;

public class PanzerLogger implements Logger {

	private List<LogListener> listeners;

	public PanzerLogger() {
		listeners = new ArrayList();
	}

	public void log(Object src, String msg) {
		LogEvent evt = new LogEvent(src, msg);
		for (LogListener listener : listeners)
			listener.performLogEvent(evt);
	}

	public void addLogListener(LogListener listener) {
		listeners.add(listener);
	}

	public void removeLogListener(LogListener listener) {
		listeners.remove(listener);
	}
}