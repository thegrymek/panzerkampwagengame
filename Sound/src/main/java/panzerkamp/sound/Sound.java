package panzerkamp.sound;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.MalformedURLException;
import java.net.URL;

import panzerkamp.main.Constans;

public class Sound {
	
	public static void play(String filename)
	{
		try {
			AudioClip clip = Applet.newAudioClip(new URL("file:" + Constans.PATH_SOUND + filename));
			clip.play();
		} catch (MalformedURLException murle) {
			System.out.println(murle);
		}
	}
		
}
