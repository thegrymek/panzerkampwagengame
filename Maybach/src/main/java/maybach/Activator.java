package maybach;

import maybach.engine.Engine;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

//    private ServiceRegistration service;
    
    public void start(BundleContext context) throws Exception {
        context.registerService(Engine.class.getName(), Engine.getEngine(), null);
        // TODO add activation code here
    }

    public void stop(BundleContext context) throws Exception {
//        this.service.unregister();
        // TODO add deactivation code here
    }

}
