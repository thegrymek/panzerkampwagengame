/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package maybach.engine;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import maybach.layer.ILayer;
import maybach.layer.Layer;
import maybach.object.Shape;

/**
 *
 * @author tkarpinski
 */
public interface IEngine {

    // Layers
    //////////////////////////////////////////////////////////////////////
    void RegisterLayer(Layer l);

    // Shapes
    //////////////////////////////////////////////////////////////////////
    void RegisterShape(Shape s);

    void RegisterShape(Shape s, String layerName);

    void Start();

    void Stop();

    void UnregisterLayer(int c);

    void UnregisterShape(Shape s);

    Shape[] getAllShapes();

    Layer getLayer();

    ILayer getLayer(String s);

    ILayer getLayer(Shape s);

    ILayer getLayer(int l);

    Shape getShapeFocus();

    Shape[] getShapes(int layer);

    boolean isShapeFocus();
    /////////////////////////////////////////////////////////////

    void paint(Graphics g);

    void run();

    // Shape focus
    ////////////////////////////////////////////////
    void setShapeFocus(Shape s);

    void translate(int dx, int dy);

    void update(long dt);

    public void setBackground(Color BLACK);

    public void addMouseListener(MouseListener sel);

    public void addMouseMotionListener(MouseMotionListener sel);

    public void addMouseWheelListener(MouseWheelListener sel);

    public void removeAll();
    
}
