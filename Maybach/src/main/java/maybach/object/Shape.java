package maybach.object;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Color;
import java.awt.Image;
import java.util.*;

import maybach.collision.*;

import maybach.engine.Engine;

public abstract class Shape extends Point implements IShape {

    private Color backColor = null;
    private Color frontColor = Color.BLACK;

    private boolean isVisible = true;

    protected double angle = 0;
    protected boolean selected = false;

    protected List<Point> collisionPoints;
    protected int side;

    protected String name = "Shape";

    // logo sprite
    protected Image logo = null;

    public Shape() {
        this(0, 0, 0);
    }

    public void setSide(int s) {
        this.side = s;
    }

    public int getSide() {
        return this.side;
    }

    public void setVisible(boolean v) {
        this.isVisible = v;
    }

    public boolean getVisible() {
        return this.isVisible;
    }

    public boolean isVisible() {
        return this.isVisible;
    }

    public String getName() {
        return name;
    }

    public Shape(int x, int y) {
        this(x, y, 0);
    }

    public Shape(int x, int y, double angle) {
        this(x, y, angle, 0);
    }

    public Shape(int x, int y, double angle, int side) {
        super(x, y);
        this.angle = angle;
        this.side = side;
        //this.collisionPoints = new ArrayList<Point>();
    }

    // Collisions
    /////////////////////////////////////////////////////////////////////
    public List<Point> getCollisionPoints() {
        return this.collisionPoints;
    }

    public void setCollisionPoints(List<Point> newCollisionPoints) {
        this.collisionPoints = (List<Point>) newCollisionPoints;
    }

    public void addCollisionPoints(Point p) {
        this.collisionPoints.add(p);
    }

    public void addCollisionPoints(int x, int y) {
        this.addCollisionPoints(new Point(x, y));
    }

    public void removeCollisionPoints(int l) {
        if (l > this.collisionPoints.size()) {
            return;
        } else {
            this.collisionPoints.remove(l);
        }
    }

    public void removeCollisionPoints(Point p) {
        this.collisionPoints.remove(p);
    }

    public void removeCollisionPoints(int x, int y) {
        this.collisionPoints.remove(new Point(x, y));
    }

    protected Map<String, String> checkCollision(String msg) {
        Map<String, String> context = new HashMap<String, String>();
        for (Shape s : Engine.getEngine().getAllShapes()) {
            if (s == this) {
                continue;
            }
            if (TestCollision.polygonInPolygon(s.getCollisionPoints(), this.getCollisionPoints())) {
                Map<String, String> res = s.getMsg(msg, this, this.getInfo());
                for (String key : res.keySet()) {
                    if (!context.containsKey(key)) {
                        context.put(key, "this");
                    }
                }

            }
        }

        return context;
    }

    public Map<String, String> getMsg(String type, Shape s, Map<String, String> dict) {
        return new HashMap<String, String>();
    }

    public Map<String, String> getInfo() {
        return new HashMap<String, String>();
    }

    ////////////////////////////////////////////////////////
    public Color getFillColor() {
        return this.backColor;
    }

    public Color getColor() {
        return this.frontColor;
    }

    public void setFillColor(Color c) {
        this.backColor = c;
    }

    public void setBackColor(Color c) {
        this.frontColor = c;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setSelected() {
        this.selected = true;
    }

    public void unSelected() {
        this.selected = false;
    }

    public void update(long dt) {
    }

    @Override
    public void paint(Graphics g) {
    }

    public void destroy() {
        //System.out.println(this.getName());
        Engine.getEngine().UnregisterShape(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == (Object) this) {
            return true;
        }
        return false;

    }

    public Image getLogo() {
        return null;
    }

    public void setLogo(Image img)
    {
        this.logo = img;
    }
}
