package maybach.object;

import java.awt.Graphics;

public interface IShape {
    public static final String NAME_PROPERTY = "simple.shape.name";
	public void paint(Graphics g);

}
