package maybach.object;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;

public class Line extends Shape {

	
	protected int x1;
	protected int y1;
	
	public Line(int x, int y, int x1, int y1)
	{
		super(x,y);
		this.name ="Line";
	}
	
	
	public void Move(int x, int y, int x1, int y1)
	{
		super.move(x, y);
		this.x1 = x1;
		this.y1 = y1;
	}

	public void Translate(int dx, int dy)
	{
		super.translate(dx, dy);
		this.x1+=dx;
		this.y1+=dx;
	}
	
	@Override
	public void paint(Graphics g)
	{
        Graphics2D g2d = (Graphics2D) g;
        Color c = g2d.getColor();
        g2d.setColor(this.getColor());
        g2d.drawLine(this.x, this.y, this.x1, this.y1);
        g2d.setColor(c);
	}
}
