package maybach.object;

import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.net.URL;

public class Sprite extends Shape {
	protected Image img;
	protected String filename;
	
	public Sprite(int x, int y, URL url)
	{

		super(x,y);	
		this.filename = filename;
		this.name = "Sprite";
		this.img = new ImageIcon(url).getImage();
	}
        
	public int getWidth()
	{
		return this.img.getWidth(null);
	}
	
	public int getHeight()
	{
		return this.img.getHeight(null);
	}
	
	public Image getImage()
	{
		return this.img;
	}
	
	@Override
	public void paint(Graphics g)
	{
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(this.img, this.x, this.y, null);
	}
}
