package maybach.object;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.*;

public class Circle extends Shape {
		
	protected double r;
	protected int destx;
	protected int desty;

	
	public Circle(int x, int y, float r)
	{
		super(x,y);
		this.r = r;
		this.name = "Circle";
		destx = x;
		desty = y;
	}
	
	@Override
	public void paint(Graphics g)
	{
        Graphics2D g2d = (Graphics2D) g;
        Color c = g2d.getColor();
        g2d.setColor(this.getColor());
        g2d.draw(new Ellipse2D.Float((float)super.getX(),(float)super.getY(), (float)r, (float)r));
        g2d.setColor(c);
	}
}
