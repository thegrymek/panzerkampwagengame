/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panzerkamp.object.tank;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Map;
import maybach.object.Shape;

/**
 *
 * @author thegrymek
 */
public interface ITank {

    void Bewegen(long dt);

    void Drehen(double winkel);

    void Feuer();

    void destroy();

    /**
     * @return the bewegtbild
     */
    Image[] getBewegtbild();

    Map<String, String> getInfo();

    /**
     * @return the logo
     */
    Image getLogo();

    Map<String, String> getMsg(String type, Shape s, Map<String, String> dict);

    // is blockAble()
    boolean istBlockierBar();

    // is destroyAble()
    boolean istZerstorBar();

    void keyPressed(KeyEvent e);

    void keyReleased(KeyEvent e);

    void keyTyped(KeyEvent arg0);

    void mouseClicked(MouseEvent e);

    void mouseDragged(MouseEvent e);

    void mouseEntered(MouseEvent e);

    void mouseExited(MouseEvent e);

    void mouseMoved(MouseEvent e);

    void mousePressed(MouseEvent e);

    void mouseReleased(MouseEvent e);

    void paint(Graphics g);

    /**
     * @param bewegtbild the bewegtbild to set
     */
    void setBewegtbild(Image[] bewegtbild);

    /**
     * @param logo the logo to set
     */
    void setLogo(Image logo);

    void update(long dt);
    
}
