/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panzerkamp.menu;

import java.awt.Component;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.EmptyStackException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import maybach.engine.Engine;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import panzerkamp.game.Game;
import panzerkamp.object.tank.ITank;
import panzerkamp.object.tank.Tank;
import panzerkamp.object.terrain.ITerrain;
import panzerkamp.object.terrain.Terrain;

/**
 *
 * @author thegrymek
 */
public class MenuFrame extends JFrame {

    private MenuFrame thisFrame;
    private BundleContext thisContext;
    private ServiceReference frameService;

    MenuFrame(String text, final BundleContext context) {
        super(text);
        thisFrame = this;
        thisContext = context;
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(800, 600);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                try {

                    frameService.getBundle().stop();
                } catch (BundleException ex) {
                    ex.printStackTrace();
                }
            }
        });

        this.setVisible(true);
        this.initComponents();
    }

    ;

    public JPanel getTankPanel() {
        return this.jPanel2;
    }

    public JPanel getMapPanel() {
        return this.jPanel4;
    }
    
    public ButtonGroup getMapGroupB()
    {
        return this.buttonGroup2;
    }

    public ButtonGroup getTankGroupB()
    {
        return this.buttonGroup1;
    }
    
    public void setService(ServiceReference ref) {
        this.frameService = ref;
    }
    // Variables declaration - do not modify                     
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration                   

    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 100, Short.MAX_VALUE));
        jPanel3Layout.setVerticalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 100, Short.MAX_VALUE));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 100, Short.MAX_VALUE));
        jPanel6Layout.setVerticalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 100, Short.MAX_VALUE));

        setBackground(new java.awt.Color(2, 2, 2));
        setMaximumSize(new java.awt.Dimension(800, 600));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setName("Panzerkampwagen Game"); // NOI18N
        setPreferredSize(new java.awt.Dimension(800, 600));
        setResizable(false);
        getContentPane().setLayout(new java.awt.GridLayout());

        ///////////////////////////////////////////////////////////////////////////////////
        // Panel Logo
        jPanel1.setBackground(new java.awt.Color(1, 1, 1));
        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setAutoscrolls(true);
        jPanel1.setLayout(new java.awt.GridLayout(5, 1));

        Image i1 = new ImageIcon(this.getClass().getClassLoader().getResource("images/logo.png")).getImage();
        ImageIcon imgIc = new ImageIcon(i1);
        jLabel2.setIcon(imgIc); // NOI18N
        jLabel2.setDebugGraphicsOptions(javax.swing.DebugGraphics.BUFFERED_OPTION);
        jLabel2.setMaximumSize(new java.awt.Dimension(800, 300));
        jLabel2.setMinimumSize(new java.awt.Dimension(800, 300));
        jLabel2.setPreferredSize(new java.awt.Dimension(800, 300));
        jPanel1.add(jLabel2);

        ///////////////////////////////////////////////////////////////////////////////////
        // Panel Wybierz Czolg
        jPanel2.setBackground(new java.awt.Color(1, 1, 1));
        jPanel2.setAlignmentX(0.0F);
        jPanel2.setAlignmentY(0.0F);
        jPanel2.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jPanel2.setMaximumSize(new java.awt.Dimension(811, 150));
        jPanel2.setMinimumSize(new java.awt.Dimension(811, 150));
        jPanel2.setName("TankPanel"); // NOI18N
        jPanel2.setPreferredSize(new java.awt.Dimension(811, 150));
        jPanel2.setRequestFocusEnabled(false);
        jPanel2.setVerifyInputWhenFocusTarget(false);
        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.LINE_AXIS));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(201, 201, 201));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Wybierz Czołg: ");
        jLabel1.setBorder(null);
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jPanel2.add(jLabel1);

        jPanel1.add(jPanel2);

        ///////////////////////////////////////////////////////////////////////////////////
        // Panel Wybierz Mape
        jPanel4.setBackground(new java.awt.Color(1, 1, 1));
        jPanel4.setName("MapPanel"); // NOI18N
        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.X_AXIS));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(221, 221, 255));
        jLabel3.setText("Wybierz Mapę: ");
        jLabel3.setAlignmentX(0.5F);
        jPanel4.add(jLabel3);

        jPanel1.add(jPanel4);

        ///////////////////////////////////////////////////////////////////////////////////
        // Panel z przyciskiem Graj
        jPanel5.setBackground(new java.awt.Color(1, 1, 1));

        jButton3.setText("Graj");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    ServiceReference[] gameList = thisContext.getServiceReferences(Game.class.getName(), null);
                    ServiceReference[] mapList = thisContext.getServiceReferences(Terrain.class.getName(), null);
                    ServiceReference[] tankList = thisContext.getServiceReferences(Tank.class.getName(), null);
                    if (gameList == null || mapList == null || tankList == null) {
                        throw new Exception("Empty service reference lists");
                    }
                    // getting game
                    Game game = (Game) thisContext.getService(gameList[0]);
                    // getting selected map
                    String mapName = buttonGroup2.getSelection().getActionCommand();
                    int sx, sy;
                    sx = 0;
                    sy = 0;
                    for (int i = 0; i < mapList.length; i++) {
                        ITerrain m = (ITerrain) thisContext.getService(mapList[i]);
                        if (m.getName().compareTo(mapName) == 0) {
                            // loading map
                            m.setMap();
                            // getting start position for tank
                            sx = m.getSx();
                            sy = m.getSy();
                            break;
                        }
                    }

                    // getting selected tank
                    String tankName = buttonGroup1.getSelection().getActionCommand();
                    for (int i = 0; i < tankList.length; i++) {
                        Tank t = (Tank) thisContext.getService(tankList[i]);
                        if (t.getName().compareTo(tankName) == 0) {
                            // setting start tank position
                            t.setX(sx);
                            t.setY(sy);
                            // loading tank
                            game.RegisterPlayer(t);
                            break;
                        }
                    }
                    game.Start();

                } catch (InvalidSyntaxException ex) {
                    Logger.getLogger(MenuFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(MenuFrame.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
                jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(557, Short.MAX_VALUE)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)));
        jPanel5Layout.setVerticalGroup(
                jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(jButton3)
                .addContainerGap(220, Short.MAX_VALUE)));

        jPanel1.add(jPanel5);

        ///////////////////////////////////////////////////////////////////////////////////
        // Panel z napisami ktos zrobil itd
        jTextArea1.setEditable(false);
        jTextArea1.setBackground(new java.awt.Color(1, 1, 1));
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jTextArea1.setForeground(new java.awt.Color(193, 193, 193));
        jTextArea1.setRows(5);
        jTextArea1.setText("PanzerKampWagen\nGra stworzona przez:\nAndrzej Grymkowski\nDamian Karpiński\nPiotr Janczewski");
        jTextArea1.setBorder(null);
        jScrollPane1.setViewportView(jTextArea1);

        jPanel1.add(jScrollPane1);

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>                        

    
    
}
