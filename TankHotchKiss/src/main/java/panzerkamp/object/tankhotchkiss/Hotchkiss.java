package panzerkamp.object.tankhotchkiss;

import java.awt.Image;

import javax.swing.ImageIcon;

import panzerkamp.main.Constans;
import panzerkamp.object.bulleteinfache.EinFacheKugel;
import panzerkamp.object.tank.Tank;

public class Hotchkiss extends Tank {

    public Hotchkiss(int x, int y, double winkel, int seite) {
        super(x, y, winkel, seite);

        this.name="HotchKiss";
        // reload speed
        this.ladenGeschwindikeit = 70;
        // fire speed
        this.feuerGeschwindigkeit = 500;
        // rotate speed
        this.drehungGeschwindigkeit = 0.3;
        // move speed
        this.bewegungsGeschwindigkeit = 18;
        // health
        this.gesundheit = 2500;
        this.maxgesundheit = 2500;
        // fire strength
        this.feuerKraft = 100;
        // shot distance
        this.schussDistanz = 300;
        // cost
        this.preis = 100;

        // kugelType
        this.geschoss = new EinFacheKugel(this.x, this.y, 0, this.getSide(), this.schussDistanz, this.feuerKraft);
        //Sprites
        bewegtbild = new Image[]{
            new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "hotchkissH35-korper.png")).getImage(),
            new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "hotchkissH35-fass.png")).getImage()
        };

        this.logo = new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "hotchkissH35.png")).getImage();
        this.updateDist(this.bewegtbild[0]);
    }

}
